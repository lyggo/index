# Personal Homepage

Hi! I’m Liu Yang, a Test Development Engineer!

Welcome to my GitCode! I specialize in backend development, automation testing, and DevOps. Here are some of my tech stacks:

---

## Tech Stack

### Backend Development
- Python
- FastAPI
- Sanic
- Flask
- Django
- Scrapy

### Frontend Development
- Vue.js
- UniApp

### Automation Testing
- Unit Testing
- Integration Testing
- Performance Testing

### DevOps
- Docker
- Docker Compose
- Kubernetes (K8s)
- Shell Scripting

---

## Dynamic Reports

Here are some dynamic reports from my GitCode projects:

![Build Status](https://img.shields.io/gitlab/pipeline/lyggo/badge.svg)
![Test Coverage](https://img.shields.io/sonar/coverage/lyggo.svg)

You can explore the code and reports at my [GitCode profile](https://gitcode.com/lyggo).

---

## Contact Me

- Email: zlkpo@hotmail.com

Thanks for visiting my personal homepage!
